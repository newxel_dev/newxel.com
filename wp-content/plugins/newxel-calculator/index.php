<?php
/*
Plugin Name: Newxel Calculator
Plugin URI: https://newxel.com/
Description: Калькулятор для сайта Newxel.
Version: 0.0.1
Author: Dmitry Miroschnichenko 
Author URI: https://twitter.com/dean992008
*/

class NewxelCalc {
    function __construct(){
        $this->init();
    }
    private function init(){
        // var_dump('tests');
        wp_enqueue_script('VueJs', plugin_dir_url(__FILE__).'assets/js/vue.js', array(), '2.3.4');
        wp_enqueue_script('NewxelCalcJs', plugin_dir_url(__FILE__).'assets/js/index.js', array(), '0.0.1');
    }
    public static function shortCode(){
        return file_get_contents(plugin_dir_path(__FILE__) . 'assets/html/layout.html');
    }
}

add_shortcode('newxelcalc', array( new NewxelCalc, 'shortCode'));
// activationhook
// deactivationhook
// uninstallhookin
