let gulp = require('gulp');
let sass = require('gulp-sass');
let csso = require('gulp-csso');
let autoprefixer = require('gulp-autoprefixer');
let stripDebug = require('gulp-strip-debug');
let uglify = require('gulp-uglify');
let flatten = require('gulp-flatten');
let importer = require('gulp-fontello-import');
let mainBowerFiles = require('gulp-main-bower-files');
let fontmin = require('gulp-fontmin');
let concat = require('gulp-concat');
let ts = require('gulp-typescript');
let strip = require('gulp-strip-comments');
let tinypng = require('gulp-tinypng-compress');
let browserSync = require('browser-sync');
let sourcemaps = require('gulp-sourcemaps');
let wpmanifest = require("gulp-wpmanifest");
let wpPot = require('gulp-wp-pot');

gulp.task('development:sass', ()=>{
	gulp.src('./bem/**/*.sass')
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('style.css'))
		.pipe(autoprefixer())
		.pipe(wpmanifest({
			name: 'NewXel',
			description: '...',
			version: 1.0,
			uri: "https://newxel.com/",
			tags: "...",
			author: {
				name: "Dmitry Miroshnichenko"
			},
			authorUri: "https://newxel.com/",
			license: "GNU General Public License v2 or later",
			licenseUri: "http://www.gnu.org/licenses/gpl-2.0.html"
		}))
	.pipe(gulp.dest('./'))
	.pipe(browserSync.stream());
});
gulp.task('development:js', ()=>{
	gulp.src(['./bem/**/*.ts', './bem/**/*.js'])
		.pipe(ts({
			target: 'ES5'
        }))
		.pipe(concat('tmpl.js'))
		.pipe(gulp.dest('./js'));
});
gulp.task('development:image', ()=>{
	gulp.src(['./bem/**/*.jpg', './bem/**/*.png', './bem/**/*.ico', './bem/**/*.svg', './bem/**/*.gif'])
		.pipe(flatten())
		.pipe(gulp.dest('./img'));
});
gulp.task('development', ['development:sass', 'development:js']);

gulp.task('production:sass', function(){
	gulp.src('./bem/**/*.sass')
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('style.css'))
		.pipe(autoprefixer())
		.pipe(csso())
		.pipe(wpmanifest({
			name: 'NewXel',
			description: '...',
			version: 1.0,
			uri: "https://newxel.com/",
			tags: "...",
			author: {
				name: "Dmitry Miroshnichenko"
			},
			authorUri: "https://newxel.com/",
			license: "GNU General Public License v2 or later",
			licenseUri: "http://www.gnu.org/licenses/gpl-2.0.html"
		}))
		.pipe(gulp.dest('./'));
});
gulp.task('production:js', function(){
	gulp.src(['./bem/**/*.ts', './bem/**/*.js'])
		.pipe(ts({
			target: 'ES5'
		}))
		.pipe(stripDebug())
		.pipe(strip())
		.pipe(concat('tmpl.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./js'));
});
gulp.task('production:image', function(){
	gulp.src(['./bem/**/*.jpg', './bem/**/*.png'])
		.pipe(flatten())
		.pipe(tinypng({
            key: 'bRxEP4uNA698Cm1QupLXYwYOQiTpxUg6',
            log: true
        }))
		.pipe(gulp.dest('./img'));
	gulp.src(['./bem/**/*.svg', './bem/**/*.ico'])
		.pipe(flatten())
		.pipe(gulp.dest('./img'));
});
gulp.task('production', ['production:sass', 'production:js']);

gulp.task('watch', function() {
	gulp.watch(['./bem/**/*.sass', './bem/**/*.js', './bem/**/*.jpg', './bem/**/*.png', './bem/**/*.ts'], ['development']);
});

gulp.task('fontgen', function () {
    gulp.src('./font-dev/*.{ttf,otf}')
        .pipe(fontmin({
			fontPath: "../font/"
		}))
        .pipe(gulp.dest('./font'));
});

gulp.task('fontgen-css', function(){
	gulp.src('./font/*.css')
		.pipe(concat('fonts.css'))
		.pipe(gulp.dest('./css/'));
});

gulp.task('bower', function(){
	gulp.src([
			'./bower_components/slick-carousel/slick/slick.min.js',
			'./bower_components/jquery/dist/jquery.min.js'
		])
		.pipe(flatten())
		.pipe(gulp.dest('./js'));
	gulp.src([
			'./bower_components/slick-carousel/slick/slick.css',
			'./bower_components/slick-carousel/slick/slick-theme.css',
		])
		.pipe(flatten())
		.pipe(csso())
		.pipe(gulp.dest('./css'));
	gulp.src([
			'./bower_components/slick-carousel/slick/ajax-loader.gif',
		])
		.pipe(flatten())
		.pipe(gulp.dest('./css'));

	gulp.src([
			'./bower_components/slick-carousel/slick/fonts/*'
		])
		.pipe(flatten())
		.pipe(gulp.dest('./css/fonts'));
});

gulp.task('fontello', function(cb) {
    importer.getFont({
        host           	: 'http://fontello.com',
        config         	: 'fontello.json',
        css 			: 'css',
        font 			: 'font'
    },cb);
});

gulp.task('serve', ['development'], function() {
    browserSync.init({
        proxy: "http://newxel/",
        // tunnel: true
    });
    gulp.watch(['./bem/**/*.sass', './bem/**/*.js', './bem/**/*.jpg', './bem/**/*.png', './bem/**/*.ts'], ['development']);
    gulp.watch(["../../../../**/*.html", "../../../../**/*.php"]).on('change', browserSync.reload);
});

gulp.task('pot', () => {
    gulp.src('./**/*.php')
        .pipe(wpPot( {
            // domain: 'domain',
            package: 'Example project'
        } ))
        .pipe(gulp.dest('../../languages/themes/newxel-en_GB.po'));
});
