<fixgure id='image-<?php echo $item; ?>' class="container">
	<img data-lazy="<?php echo wp_get_attachment_image_src($item, $atts['size'], false, array('class' => 'img-responsive'))[0]; ?>" alt="" />
	<?php if($meta->post_excerpt){ ?>
	<div class="row">
		<div class="col-lg-6 col-md-9 col-xs-12">
			<div class="slick__slide__title"><?php echo $meta->post_title; ?></div>
			<div class="slick__slide__subscribe"><?php echo $meta->post_excerpt; ?></div>
			<div class="slick__slide__desc"><?php echo $meta->post_content; ?></div>
			<a href="#" class="slick__slide__link"><?php _e('More about us', 'newxel'); ?></a>
		</div>
	</div>
	<?php } ?>
</figure>
