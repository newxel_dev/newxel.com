<?php
	$slick_atts["pauseOnDotsHover"] = false;
	$slick_atts["pauseOnFocus"] = false;
	$slick_atts["pauseOnHover"] = false;
	if($slick_atts['rtl']){
		$slick_atts['rtl'] = true;
	}
	unset($slick_atts['useSlick']);
	$slick_attr = "data-slick='".json_encode($slick_atts) . "'";
?>
<div <?php echo ($slick_atts['rtl'])?'dir="rtl"':'';?> class='<?php echo $slick_class; ?>' <?php echo $slick_attr; ?>>
	<div><?php echo implode('</div><div>', $slick_slides); ?></div>
</div>
