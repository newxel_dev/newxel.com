<?php
	if (function_exists('add_theme_support')) {
		add_theme_support('menus');
		add_theme_support('post-thumbnails');
		add_theme_support('custom-logo', array(
			'height'      => 45,
			'width'       => 264,
			'flex-height' => true,
			'flex-width'  => true,
		));
	}
	add_image_size( 'who-we-are-bigger', 555, 370, true );
	add_image_size( 'who-we-are-lettle', 341, 227, true );
	add_image_size( 'mobile-size', 360, 640 );
	add_image_size( 'mobile-size-n5', 384, 640 );
	add_image_size( 'mobile-size-ip6p', 414, 736 );
	function ths_scripts() {
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . "/css/bootstrap.min.css");
		wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri() . "/css/bootstrap-theme.min.css");
		if($_SESSION["language"] == "he_IL"){
			wp_enqueue_style( 'bootstrap-rtl', get_template_directory_uri() . "/css/bootstrap-rtl.min.css");
		}
		// wp_enqueue_style( 'fonts', "https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=cyrillic");
		wp_enqueue_style( 'fonts', get_template_directory_uri() . "/css/fonts.css");
		wp_enqueue_style( 'animate', get_template_directory_uri() . "/css/animate.css");
		wp_enqueue_style( 'main', get_template_directory_uri() . "/style.css");
		wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array(), '1.1.2', true );
		wp_enqueue_script( 'main', get_template_directory_uri() . '/js/tmpl.js', array(), '1.0.0', true );
		// wp_enqueue_script( 'yandexShare', '//yastatic.net/share2/share.js', array(), '1.15.3', true);
		wp_enqueue_script( 'yandexShare', get_template_directory_uri() . '/js/share.js', array(), '1.15.3', true);
		wp_enqueue_script( 'dateFomated',  get_template_directory_uri() . '/js/jquery-dateFormat.min.js', array(), '0.0.1', true);
	}
	function remove_more_jump_link($link) {
		$offset = strpos($link, '#more-');
		if ($offset) {
			$end = strpos($link, '"',$offset);
		}
		if ($end) {
			$link = substr_replace($link, '', $offset, $end-$offset);
		}
		return $link;
	}
	function remove_head_scripts() { 
		remove_action('wp_head', 'wp_print_scripts'); 
		remove_action('wp_head', 'wp_print_head_scripts', 9); 
		remove_action('wp_head', 'wp_enqueue_scripts', 1);

		add_action('wp_footer', 'wp_print_scripts', 5);
		add_action('wp_footer', 'wp_enqueue_scripts', 5);
		add_action('wp_footer', 'wp_print_head_scripts', 5); 
	} 
	
	add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );
	add_filter('the_content_more_link', 'remove_more_jump_link');
	add_action( 'wp_enqueue_scripts', 'ths_scripts' );
?>
