	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="footer__title"><?php _e('About us', 'newxel'); ?></div>
					<div class="footer__text"><?php _e('Newxel is a company that helps to connect your business with talented developers in Ukraine for your digital transformations.', 'newxel'); ?></div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="footer__title"><?php _e('Contacts', 'newxel'); ?></div>
					<div class="footer__text"><?php _e('132 Holosiivskyi avenue, office 35, Kyiv, Ukraine, 03127', 'newxel'); ?></div>
					<div class="footer__social">
						<a target="_blank" href="https://www.facebook.com/newxel">&#xe800;</a>
						<a target="_blank" href="https://www.linkedin.com/company-beta/16194441/">&#xe801;</a>
						<a target="_blank" href="https://twitter.com/newxel">&#xe804;</a>
					</div>
				</div>
				<div class="col-lg-4 col-lg-offset-2 col-md-5 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12 footer__newsletter">
					<div class="footer__title"><?php _e('Newsletter', 'newxel'); ?></div>
<!--					<form action="">-->
						<?php /*
						<input type="text" placeholder="Email">
						<button><?php _e('Subscribe', 'newxel'); ?></button>
						*/?>
						<?php echo do_shortcode('[contact-form-7 id="29" title="Footer"]'); ?>
<!--					</form>-->
					<div class="footer__text"><?php _e('No spam, unsubscribe at any time', 'newxel'); ?></div>
				</div>
			</div>
			<div class="row footer__bottom">
				<div class="col-md-6 col-xs-12 footer__copyright">© Newxel <?php echo date("Y"); ?>. <?php _e('All rights reserved.', 'newxel'); ?></div>
				<div class="col-md-6 col-xs-12 footer__design">
					<?php _e('Design by', 'newxel'); ?>
					<img src="<?php echo get_template_directory_uri() ?>/img/footer__author-logo.png" alt="" />
				</div>
			</div>
		</div>

	</footer>
	<?php wp_footer(); ?>
</body>
</html>
