<?php get_header(); ?>
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
	<div class="container">
		<?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>
	</div>
</div>
<div class="blog container">
    <div class="col-md-10 col-md-offset-1 col-xs-12 blog__content">
        <div class="blog__content__date"><?php the_time('M d, Y') ?></div>
        <h1 class="blog__content__title"><?php the_title(); ?></h1>
        <div class="blog__content__img"><?php the_post_thumbnail(); ?></div>
        <div class="blog__content__text">
            <?php the_content(); ?>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="blog__content__author"><?php _e('By', 'newxel'); ?> <span><?php echo get_the_author_meta('display_name', get_post_field( 'post_author', get_the_ID())); ?></span></div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="blog__content__tags">
                    <?php the_tags('', ''); ?>
                </div>
            </div>
        </div>
        <div class="ya-share2"
        	data-services="facebook,twitter,linkedin"
        	data-title="<?php the_title(); ?>"
        	data-image="<?php the_post_thumbnail_url('full'); ?>"
        	data-bare
        	data-counter
        ></div>
    </div>
</div>
<?php get_footer(); ?>
