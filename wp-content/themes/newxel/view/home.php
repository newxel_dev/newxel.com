<?php
/*
Template Name: Главная
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
	<?php the_content(); ?>
	<div class="why-ukraine">
		<div class="block-title" data-wow-duration="0.5s">
			<div class="block-title__title"><?php _e('Why Ukraine?', 'newxel'); ?> </div>
			<div class="block-title__subtitle"><?php _e('Three most powerful reasons for starting work in Ukraine', 'newxel'); ?></div>
		</div>
		<div class="container">
			<div class="col-md-5 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12 why-ukraine__list">
				<ul>
					<?php
						$whyUkraine = get_posts(array(
							'category' => 11,
							'post_type' => 'post',
							'orderby' => 'post_id',
							'order' => 'ASC'
						));
					?>
					<?php foreach ( $whyUkraine as $post ){ ?>
						<?php setup_postdata( $post ); ?>
						<?php $image = get_attached_file(get_post_thumbnail_id(get_the_ID())); ?>
						<li class="wow fadeInUp" data-wow-duration="0.5s">
							<div class="why-ukraine__list__icon">
								<?php echo file_get_contents($image); ?>
							</div>
							<div class="why-ukraine__list__title"><?php the_title(); ?></div>
							<div class="why-ukraine__list__text"><?php the_content(); ?></div>
						</li>
					<?php } ?>
				</ul>
			</div>
			<div class="col-lg-6 col-lg-offset-1 col-md-7 col-md-offset-0 hidden-sm hidden-xs why-ukraine__map">
				<img src="<?php echo get_template_directory_uri() ?>/img/why-ukraine__map.png" alt="" />
				<div class="why-ukraine__map__dot" style="top:13.6%;left:54.91%;width: 15px;height: 15px;"></div>
				<div class="why-ukraine__map__dot" style="top:18.87%;left:10.7%;width: 9px;height: 9px;"></div>
				<div class="why-ukraine__map__dot" style="top: 48.46%;left: 42.28%;width: 9px;height: 9px;"></div>
				<div class="why-ukraine__map__dot" style="top: 32.31%;left: 68.94%;width: 9px;height: 9px;"></div>
				<div class="why-ukraine__map__dot" style="top: 24.14%;left: 77.36%;width: 9px;height: 9px;"></div>
			</div>
		</div>
	</div>
	<div class="who-we-are menu-watch" id="who-we-are">
		<div class="block-title">
			<div class="block-title__title"><?php _e('Who we are', 'newxel'); ?></div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-6 col-sm-6 who-we-are__imgbox">
					<?php 
						$WhoWeAreSlides = new WP_Query(array(
							'post_type' => 'page',
							'page_id'	=> 182
						));
						while ( $WhoWeAreSlides->have_posts() ) {
							$WhoWeAreSlides->the_post();
							the_content();
						}
					?>
				</div>
				<div class="col-lg-5 col-md-6 col-sm-6 who-we-are__list">
					<ul>
						<?php
							$whoWeAre = get_posts(array(
								'category' => 12,
								'post_type' => 'post',
								'orderby' => 'post_id',
								'order' => 'ASC'
							));
						?>
						<?php foreach ( $whoWeAre as $post ){ ?>
							<?php setup_postdata( $post ); ?>
							<?php $image = get_attached_file(get_post_thumbnail_id(get_the_ID())); ?>
							<li class="wow fadeInRight"  data-wow-duration="0.5s"><?php the_content(); ?></li>
						<?php } ?>
					</ul>
					<a href="/#contact-us" class="button"><?php echo _e('Work with us', 'newxel'); ?></a>
				</div>
			</div>
		</div>
	</div>
	<div class="what-we-offer menu-watch clearfix" id="what-we-offer">
		<div class="block-title">
			<div class="block-title__title"><?php _e('What we Offer', 'newxel'); ?></div>
			<div class="block-title__subtitle"><?php _e('Newxel sets up and supports an offshore development team or R&D center in Ukraine customized for needs', 'newxel'); ?></div>
		</div>
		<div class="container">
			<?php
				$whatWeOfferPosts = get_posts(array(
					'category' => 3,
					'post_type' => 'post',
					'orderby' => 'post_id',
					'order' => 'ASC'
				));
			?>
			<?php foreach ( $whatWeOfferPosts as $post ){ ?>
				<?php setup_postdata( $post ); ?>
				<?php $image = get_attached_file(get_post_thumbnail_id(get_the_ID())); ?>
				<div class="col-sm-4 col-xs-12">
					<div class="what-we-offer__offer-block">
						<div class="what-we-offer__offer-block__img">
							<?php echo file_get_contents($image); ?>
						</div>
						<div class="what-we-offer__offer-block__title"><?php the_title(); ?></div>
						<div class="what-we-offer__offer-block__text"><?php the_content(); ?></div>
						<div class="what-we-offer__offer-block__plus"><?php echo _e('More', 'newxel'); ?></div>
					</div>
				</div>
			<?php } ?>
			<div class="col-sm-4 col-xs-12">
				<div class="what-we-offer__more">
					<a href="/#contact-us"><?php echo _e('Work with us', 'newxel'); ?></a>
				</div>
			</div>
		</div>
	</div>

	<div class="who-is-it-for menu-watch" data-id="what-we-offer">
		<div class="block-title">
			<div class="block-title__title"><?php _e('Who is This for', 'newxel'); ?></div>
		</div>
		<div class="container">
			<?php
			$whoIsItForPosts = get_posts(array(
				'category' => 4,
				'post_type' => 'post',
				'orderby' => 'post_id',
				'order' => 'ASC'
			));
			?>
			<?php foreach ( $whoIsItForPosts as $post ){ ?>
			<div class="who-is-it-for__block">
				<?php the_post_thumbnail(); ?>
				<span class="who-is-it-for__block__title"><?php the_title(); ?></span>
				<span class="who-is-it-for__block__desc"><?php the_content(); ?></span>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="how-it-works menu-watch" id="how-it-works">
		<div class="block-title">
			<div class="block-title__title"><?php _e('How it Works', 'newxel'); ?></div>
		</div>
		<div class="how-it-works__circle hidden-sm hidden-xs">
			<div class="container">
				<?php
					$howitWorks = get_posts(array(
						'category' => 13,
						'post_type' => 'post',
						'orderby' => 'post_id',
						'order' => 'ASC'
					));
				?>
				<?php foreach ( $howitWorks as $key => $post ){ ?>
					<?php setup_postdata( $post ); ?>
					<div class="how-it-works__circle__block">
						<div class="how-it-works__circle__block__title"><?php the_title(); ?></div>
						<div class="how-it-works__circle__block__desc"><?php the_content(); ?></div>
						<div class="how-it-works__circle__block__num"><?php echo $key + 1; ?></div>
						<div class="how-it-works__circle__block__circle"></div>
					</div>
				<?php } ?>
				<div class="how-it-works__circle__dot how-it-works__circle__dot_top"></div>
				<div class="how-it-works__circle__dot how-it-works__circle__dot_left"></div>
				<div class="how-it-works__circle__dot how-it-works__circle__dot_right"></div>
				<div class="how-it-works__circle__dot how-it-works__circle__dot_bottom"><?php _e('solution', 'newxel'); ?></div>
			</div>
		</div>
		<div class="how-it-works__mobile clearfix visible-sm visible-xs">
			<div>
				<?php foreach ( $howitWorks as $key => $post ){ ?>
					<?php setup_postdata( $post ); ?>
					<div class="how-it-works__mobile__block">
						<div class="how-it-works__mobile__block__line"></div>
						<div class="how-it-works__mobile__block__num"><?php echo $key + 1; ?></div>
						<div class="how-it-works__mobile__block__title"><?php the_title(); ?></div>
						<div class="how-it-works__mobile__block__desc"><?php the_content(); ?></div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="contact-us clearfix" id="contact-us">
		<div class="block-title">
			<div class="block-title__title"><?php _e('Contact us', 'newxel'); ?></div>
		</div>
		<div class="container">
			<div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12 contact-us__form-box" >
				<?php echo do_shortcode('[contact-form-7 id="28" title="tell us"]'); ?>
			</div>
			<div class="col-lg-8 col-md-8 hidden-sm hidden-xs contact-us__map">
				<img src="<?php echo get_template_directory_uri() ?>/img/contact-us__map.png" alt="" />
				<div class="contact-us__map__dot" style="top: 51.37%;left: 32.73%;"></div>
				<div class="contact-us__map__dot" style="top: 18.34%;left: 14.5%;"></div>
				<div class="contact-us__map__dot" style="top: 33%;left: 26%;"></div>
				<div class="contact-us__map__dot" style="top: 56.01%;left: 51.28%;"></div>
				<div class="contact-us__map__dot" style="top: 36%;left: 54.5%;"></div>
				<div class="contact-us__map__dot" style="top: 53.25%;left: 78.5%;"></div>
				<div class="contact-us__map__dot" style="top: 33%;left: 80.55%;"></div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
