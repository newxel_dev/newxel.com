/// <reference path="../d.ts/jquery.d.ts"/>
declare var WOW:any

class Common {
    constructor() {
		this.wowStart();
		if(jQuery(window).width() <= 1024){
			// this.mobileActiveChecker();
			jQuery(window).scroll(this.mobileActiveChecker.bind(this));
		}
    }
    wowStart() {
        var wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 150,
            mobile: true,
            live: true,
            scrollContainer: null
        });
        wow.init();
    }
	mobileActiveChecker(){
		let top = jQuery(window).scrollTop() + jQuery(window).height() * 0.25;
        let bottom = jQuery(window).scrollTop() + jQuery(window).height() * 0.75;
        let blocks = jQuery('.what-we-offer__offer-block, .who-is-it-for__block');
        blocks.removeClass('mobile-active');
        blocks.each((i, e)=>{
		    let th = jQuery(e);
            let itemTop = th.offset().top;
            let itemBottom = itemTop + th.height();
		    if(itemTop < bottom && itemBottom > top){
		        th.addClass('mobile-active');
                return false;
            }
        });
	}
}

jQuery(function(){
	new Common();
});
