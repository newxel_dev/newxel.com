class slick{
    constructor(){
        this.setLoader();
        jQuery(document).on('click', '.slick__slide__link', this.moreAboutUs);
        jQuery('.slick').on('afterChange', this.loaderAnimate);
    }
    loaderUrl: string = "wp-content/themes/newxel/img/slick__load.svg";
    loaderXml: any;

    setLoader(){
        jQuery.ajax({
            url: this.loaderUrl,
            success: (data) => {
                jQuery('.slick').each((i, e)=>{
                    jQuery(e).find('.slick-dots').children().append(new XMLSerializer().serializeToString(data.documentElement));
                    jQuery(e).find('.slick-dots').find('.slick-active').find('svg').animate({'stroke-dashoffset': 0}, 5000);
                }); 
            }
        });
    }
    loaderAnimate(e: any, s: any, c: any, n: any){
        jQuery(e.target).find('.slick-dots').find('svg').removeAttr('style');
        jQuery(e.target).find('.slick-dots').find('.slick-active').find('svg').animate({'stroke-dashoffset': 0}, s.options.autoplaySpeed);
    }
    moreAboutUs(){
        let scroll = jQuery('#who-we-are').offset().top;
        jQuery('html, body').animate({scrollTop: scroll}, 750);   
    }
}
jQuery(function(){
    new slick();
});