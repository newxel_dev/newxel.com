class wpcd7From{
	constructor(){
		this.fileInit();
		jQuery(document).on('keyup', '.wpcf7 textarea', this.textAreaAutoHeight);
		jQuery(document).on('focus', '.wpcf7 input, .wpcf7 textarea', this.inputFocus);
		jQuery(document).on('blur', '.wpcf7 input, .wpcf7 textarea', this.inputBlur);
		jQuery(document).on('change', '.wpcf7 input, .wpcf7 textarea', this.inputValide.bind(this));
		jQuery(document).on('change', '.wpcf7 input.wpcf7-file', this.fileChange);
		jQuery(document).on('click', '.wpcf7-form-control.wpcf7-submit', this.beforeSubmit.bind(this));
	}
	textAreaAutoHeight(e:any){
		let newHeight = e.currentTarget.scrollHeight;
		e.currentTarget.style.height = (newHeight)+"px";
		if(newHeight >= 200){
			jQuery(e.currentTarget).addClass('max');
		} else {
			jQuery(e.currentTarget).removeClass('max');
		}
	}
	inputFocus(e:any){
		jQuery(e.currentTarget)
			.parent()
			.addClass('focus')
			.addClass('dirty')
			.removeClass('error');
		jQuery(e.currentTarget)
			.parent()
			.next('.wpcf7-not-valid-tip')
			.remove();
	}
	inputBlur(e:any){
		jQuery(e.currentTarget).parent().removeClass('focus');
		if(!jQuery(e.currentTarget).val().length){
			jQuery(e.currentTarget).parent().removeClass('dirty');
		}
	}
	inputValide(e:any){
		let c = jQuery(e.currentTarget);
		let type = c.attr('type');
		if(type == 'email'){
			let reg = /'^([_a-z0-9]+[\._a-z0-9]*)(\+[a-z0-9]+)?@(([a-z0-9-]+\.)*[a-z]{2,4})$/;
			if(!reg.test(c.val())){
				this.inputNotValid(e);
				this.inputNotValidMessage(e, 'incorrect email address');
				return false;
			}
		}
		if(type == 'tel'){
			let reg = /^[0\+][0-9 \.\-\/]*$/g;
			if(!reg.test(c.val())){
				this.inputNotValid(e);
				this.inputNotValidMessage(e, 'incorrect phone number');
				return false;
			}
		}
		// this.inputNotValid(e);
	}
	inputNotValid(e:any){
		jQuery(e.currentTarget).parent().addClass('error');
	}
	inputNotValidMessage(e:any, text:string){
		// <span role="alert" class="wpcf7-not-valid-tip">The field is required.</span>
		jQuery('<span/>')
			.attr('role', 'alert')
			.addClass('wpcf7-not-valid-tip')
			.text(text)
			.insertAfter(jQuery(e.currentTarget).parent());
	}
	fileInit(){
		jQuery('.user-file').append('<p class="cover">Attach File</p>');
	}
	fileChange(e:any){
		// console.log(jQuery(e.currentTarget).val());
		let c = jQuery(e.currentTarget);
		let fileName = c.val().split('\\').pop();
		c.parent().find('.cover').text(fileName).addClass('selected');
	}
	beforeSubmit(e:any){
		let valid = true;
		jQuery('.wpcf7-not-valid-tip').remove();
		jQuery(e.currentTarget)
			.parents('form')
			.find('.wpcf7-validates-as-required')
			.each((i, elem)=>{
				if(!jQuery(elem).val().length){
					let ev = {currentTarget: elem};
					this.inputNotValid(ev);
					this.inputNotValidMessage(ev, 'This item required');
					valid = false;
				}
			});
		if(!valid){
			return false;
		}
	}
}
jQuery(function(){
	new wpcd7From();
});
