class Header{
	constructor(){
		jQuery(document).on('click', '.header .header__mobi-menu', this.menuToggle);
		jQuery(document).on('click', '#menu-main li a, .header__contacts a, .what-we-offer__more a, .who-we-are__list a.button', this.menuLyft);
		if(jQuery('body').hasClass('is-home')){
			jQuery(window).scroll(this.scrollWatch.bind(this));
			this.init();
		}
	}
	init(){
		jQuery("#menu-main").find('.current-menu-item').removeClass('current-menu-item');
		this.scrollWatch();
	}
	menuToggle(){
		if(jQuery(window).width() < 992){
			jQuery('.header').toggleClass('header_menu-active');
		}
	}
	menuLyft(e:any){
		let href = jQuery(e.currentTarget).attr('href').slice(1);
		jQuery('.header').removeClass('header_menu-active');
		if(!jQuery(e.currentTarget).parent().hasClass('menu-item-home') && !jQuery(href).length) return;
		if(href.charAt(0) != "#"){
			if(jQuery(e.currentTarget).parent().hasClass('menu-item-home') && jQuery('body').hasClass('is-home')){
				jQuery('html, body').animate({scrollTop: 0}, 750);
				e.preventDefault();
				return false;
			}
			return;
		} else {
			let scroll = jQuery(href).offset().top;
			jQuery('html, body').animate({scrollTop: scroll - 80}, 750);
		}
		return false;
	}
	scrollWatch(){
		let header = jQuery('.header');
		if(jQuery(window).scrollTop() > 10 || header.hasClass('header_white')){
			header.removeClass('header_transparent');
		} else {
			header.addClass('header_transparent');
		}
		this.menuWatcher();
	}
	menuWatcher(){
		let screenTop = jQuery(window).scrollTop();
		let screenBottom = screenTop + jQuery(window).height();
		jQuery('#menu-main').find('li').removeClass('current-menu-item');
		jQuery('.menu-watch').each((i, e)=>{
			let th = jQuery(e);
			let top = th.offset().top;
			let bottom = top + th.height();
			if(top < screenBottom && bottom > screenTop){
				let selector = th.attr('id') || th.data('id');
				jQuery('#menu-main').find('[href*=' + selector + ']').parent().addClass('current-menu-item');
				return false;
			}
		});
		if(screenTop < jQuery(window).height() && jQuery('body').hasClass('is-home')){
			jQuery('#menu-main').find('.menu-item-home').addClass('current-menu-item');
		}
	}
}

jQuery(()=>{
	new Header();
});
