interface JQuery{ 
    slick(options:any):any; 
}
class howItWorks{
	scrollTimeout:any;
	constructor(){
		this.init();
	}
	init(){
		jQuery('.how-it-works__mobile > div').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrow: false,
			infinite: false
		})
	}
	scrollCheck(){
		clearTimeout(this.scrollTimeout);
		this.scrollTimeout = setTimeout(()=>{
			// console.log(jQuery('.how-it-works__mobile').scrollLeft());
			let totalWidth = jQuery('.how-it-works__mobile')[0].scrollWidth;
			let scrollPos = jQuery('.how-it-works__mobile').scrollLeft();
			let present = Math.round(scrollPos / totalWidth * 4);
			// console.log(present);
			jQuery('.how-it-works__mobile').animate({scrollLeft: present / 4 * totalWidth}, 400, ()=>{
				jQuery('.how-it-works__mobile__block').removeClass('active');
				jQuery('.how-it-works__mobile__block').eq(present).addClass('active');
			});
		}, 450);
	}
}
jQuery(()=>{
	new howItWorks();
});
