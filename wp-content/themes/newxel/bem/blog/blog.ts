interface JQueryStatic {
    format: any
}

class Blog {
    constructor() {
        this.checkMorePosts();
        jQuery(document).on('click', '.blog__load-more', this.getMorePosts.bind(this));
    }
    requestData() {
        let blogData = jQuery('.blog').data();
        return jQuery.ajax({
            method: "GET",
            url: "/wp-json/wp/v2/posts?" + blogData.taxonomy + "=" + blogData.term_id,
        });
    }
    checkMorePosts() {
        this.requestData().done(data => {
            let visible = jQuery('.blog').find('.blog__item').length;
            let total = data.length;
            if (total > visible) {
                jQuery('.blog__load-more').removeClass('hide');
            } else {
                jQuery('.blog__load-more').addClass('hide');
            }
        });
    }
    getMorePosts() {
        this.requestData().done(data => {
            let visible = jQuery('.blog').find('.blog__item').length;
            let posts = data.slice(visible, visible + 5);
            for (let i = 0; i < posts.length; i++) {
                let item = posts[i];
                let newBlock = `<div class="col-md-10 col-md-offset-1 col-xs-12 blog__item">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 blog__item__img">
                                <img src="${item.better_featured_image.source_url}" alt="">
                            </div>
                            <div class="col-sm-6 col-xs-12 blog__item__data">
                                <div class="blog__item__data__date">${jQuery.format.date(item.date, "MMM dd, yyyy")}</div>
                                <h3>${item.title.rendered}</h3>
                                <div class="blog__item__data__text">${item.content.rendered.replace('(more&hellip;)', 'Read more')}</div>
                            </div>
                        </div>
                    </div>`;
                let code = jQuery(newBlock).find('.blog__item__data__text');
                let link = code.find('a').clone();
                let textWidthOutTags = code.text().replace(' Read more', '');
                // console.log(link);
                jQuery('.blog').find('.blog__item').last().after(newBlock);
                jQuery('.blog').find('.blog__item').last().find('.blog__item__data__text').text(textWidthOutTags).append(link);
            }
            this.checkMorePosts();
        });
        return false;
    }
}

jQuery(() => {
	if(jQuery('.blog').length){
		new Blog();
	}
});
