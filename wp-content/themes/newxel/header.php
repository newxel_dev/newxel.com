<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WXT9XM6');</script>
    <!-- End Google Tag Manager -->
	<title><?php wp_title("|", true, 'right'); ?></title>
	<?php wp_head(); ?>
</head>
<body class="<?php echo (is_front_page())?'is-home':''; ?>">
	<!-- Google Tag Manager (noscript) -->
	<noscript class="hide"><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WXT9XM6"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	<div class="hide"></div>
	<header class="header header_<?php echo (is_front_page())?'transparent':'white'; ?>">
		<div class="container">
			<div class="row">
				<div class="header__logo col-md-2 col-sm-3 col-xs-5">
					<?php if ( function_exists( 'the_custom_logo' ) ) {
						the_custom_logo();
					} ?>
				</div>
				<div class="header__mobi-menu col-xs-6 col-sm-offset-3 col-xs-offset-1">
					<a href="#">mobi_menu</a>
				</div>
				<div class="header__menu col-md-6 col-md-offset-1 col-xs-12">
					<?php wp_nav_menu(array(
						'menu' => 'main',
					)); ?>
				</div>
				<div class="header__lang col-md-1 col-md-offset-0 col-xs-5 col-xs-offset-1 ">
					<?php //echo do_shortcode('[multilanguage_switcher]'); ?>
				</div>
				<div class="header__contacts col-md-2 col-xs-6">
					<a href="/#contact-us" class="button"><?php _e('Contact us', 'newxel'); ?></a>
				</div>
			</div>
		</div>
	</header>
