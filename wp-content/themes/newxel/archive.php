<?php get_header(); ?>
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
	<div class="container">
		<?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>
	</div>
</div>
<?php
	if(get_queried_object()->taxonomy == "post_tag"){
		$tax = "tags";
	} else if(get_queried_object()->taxonomy == "category"){
		$tax = "categories";
	}
?>
<div class="blog container" data-taxonomy="<?php echo $tax; ?>" data-term_id="<?php echo get_queried_object()->term_id; ?>" data-readMore="<?php _e('Read more', 'newxel'); ?>">
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-xs-12 blog__item">
                <div class="row">
                    <div class="col-sm-6 col-xs-12 blog__item__img">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="col-sm-6 col-xs-12 blog__item__data">
                        <div class="blog__item__data__date"><?php the_time('M d, Y') ?></div>
                        <h3><?php the_title(); ?></h3>
                        <div class="blog__item__data__text"><?php echo strip_tags(get_the_content(__('Read more',  'newxel')), '<a>'); ?></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
    <button class="blog__load-more hide"><?php _e('Load more', 'newxel'); ?></button>
</div>
<?php get_footer(); ?>
