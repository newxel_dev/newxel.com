<?php get_header(); ?>
<div class="index-block container">
	<?php
		while ( have_posts() ) {
			the_post();
			the_content();
		}
	?>
</div>
<?php get_footer(); ?>
